export const APP = 'APP';
export const BOOK = 'BOOK';
export const SLOT = 'SLOT';
export const EDIT = 'EDIT';
export const ROOT = 'ROOT';
export const CONTENT = 'CONTENT';
export const PARENT = 'PARENT';
export const RESOURCE = 'RESOURCE';

export const MODEL = 'MODEL';

export const SERVICES = 'SERVICES';
export const BACKUP = 'BACKUP';
export const HISTORY = 'HISTORY';
export const STORAGE = 'STORAGE';
