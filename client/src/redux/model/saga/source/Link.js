class Link {
  constructor({ up, down }) {
    this.up = Number(up);
    this.down = Number(down);
  }
}

export default Link;
