const express = require('express');
const mapDb = require('../../db/map');

const router = express.Router();

router.get('/', async (req, res) => {
  const sync = Date.now();
  const values = await mapDb.all();
  const keyValues = values
    .filter((kv) => kv.value)
    .map((x) => ({
      key: x.path,
      time: x.time,
      value: x.value,
    }));
  res.send({ sync, keyValues });
});

router.post('/', async (req, res) => {
  const { sync, keyValues } = req.body;
  const newSync = Date.now();
  const newValues = await mapDb.all({ time: sync });
  const newKeyValues = newValues
    // .filter((kv) => kv.value)
    .map((x) => ({
      key: x.path,
      time: x.time,
      value: x.value,
    }));
  await Promise.all(keyValues
    .map((x) => mapDb.upsert({
      path: x.key,
      time: x.time,
      value: x.value,
    })));
  res.send({
    sync: newSync,
    keyValues: newKeyValues,
  });
});

module.exports = router;
