const express = require('express');
const bodyParser = require('body-parser');
const bearerToken = require('express-bearer-token');
const cors = require('cors');
const config = require('config');
const routes = require('./routes');


async function run() {
  const app = express();
  app.use(cors());
  const passkey = config.get('api.passkey');
  app.get('/session', (req, res) => {
    if (passkey === req.query.passkey) {
      res.send({ token: config.get('api.token') });
    } else {
      console.log(`unknown passkey: ${req.query.passkey}`);
      res.sendStatus(401);
    }
  });
  app.use(bearerToken());
  app.use((req, res, next) => {
    if (req.token !== config.get('api.token')) {
      res.sendStatus(401);
    }
    next();
  });
  app.use(bodyParser.json({ limit: '10mb' }));
  app.use(routes);
  app.listen(process.env.PORT || config.get('api.port'));
  console.log('api run');
}

module.exports = {
  run,
};
