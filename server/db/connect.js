const sqlite = require('sqlite');
const config = require('config');


module.exports = sqlite.open(config.get('db.path'));
