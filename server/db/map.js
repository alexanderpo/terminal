const connect = require('./connect');


async function all({ time } = {}) {
  const db = await connect;
  const params = [];
  const conditions = [];
  if (typeof time === 'number') {
    conditions.push('time > ?');
    params.push(time);
  }
  let condition = '';
  if (conditions.length > 0) {
    condition = ` WHERE ${conditions.join(' AND ')}`;
  }
  return db.all(`SELECT * FROM Map${condition}`, ...params);
}

async function get({ path }) {
  const db = await connect;
  return db.get('SELECT * FROM Map WHERE path = ?', path);
}

async function insert({
  path,
  time,
  value,
}) {
  const db = await connect;
  return db.run(
    `INSERT INTO Map (
      path,
      time,
      value)
    VALUES (?, ?, ?)`,
    path,
    time,
    value,
  );
}

async function update({ path, time, value }) {
  const db = await connect;
  return db.run(
    'UPDATE Map SET time = ?, value = ? WHERE path = ?',
    time,
    value,
    path,
  );
}

async function upsert({ path, time, value }) {
  const db = await connect;
  return db.run(
    'INSERT INTO Map(path, time, value) VALUES(?, ?, ?) ON CONFLICT(path) DO UPDATE SET time = ?, value = ?',
    path,
    time,
    value,
    time,
    value,
  );
}

module.exports = {
  all,
  get,
  insert,
  update,
  upsert,
};
