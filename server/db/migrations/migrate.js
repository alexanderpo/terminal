const sqlite = require('sqlite');
const fs = require('fs');
const path = require('path');
const config = require('config');


const run = async () => {
  try {
    const dir = path.dirname(config.get('db.path'));
    if (!fs.existsSync(dir)) {
      fs.mkdirSync(dir);
    }
    const dbPromise = sqlite.open(config.get('db.path'));
    const db = await dbPromise;
    await db.migrate({ migrationsPath: './db/migrations' });
  } catch (err) {
    console.log(err.message);
  }

  console.log('done');
};

run();
