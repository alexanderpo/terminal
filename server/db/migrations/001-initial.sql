--------------------------------------------------------------------------------
-- Up
--------------------------------------------------------------------------------

CREATE TABLE Map (
  id        INTEGER PRIMARY KEY AUTOINCREMENT,
  path      TEXT NOT NULL,
  time      NUMERIC NOT NULL,
  value     TEXT
);
CREATE UNIQUE INDEX Map_ix_path ON Map (path);

--------------------------------------------------------------------------------
-- Down
--------------------------------------------------------------------------------

DROP INDEX Map_ix_path;
DROP TABLE Map;