const api = require('./api/index');

(async function run() {
  await api.run();
}());
